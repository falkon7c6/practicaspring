package com.falkon.springTutorial.Beans;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.falkon.springTutorial.beans.Persona;

import junit.framework.Assert;

public class PersonaTest {

	private Persona persona;
	
	@Before
	public void setUp() throws Exception {
		this.persona = new Persona(4, "Apolo", "Creed");
	}

	@Test
	public void testPersona() {
		Assert.assertEquals(4, persona.getId());
	}

}
