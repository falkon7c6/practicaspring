package com.falkon.springTutorial.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Bean
	public HelloWorld mundo(){
		return new HelloWorld();
	}
}
