package com.falkon.springTutorial;

import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.falkon.springTutorial.beans.AppConfig;
import com.falkon.springTutorial.beans.HelloWorld;
import com.falkon.springTutorial.beans.Persona;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
       ApplicationContext context =  new ClassPathXmlApplicationContext("com/falkon/xml/beans.xml");
    	//ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    	//en caso de tener varios archivos de configuracion
    	//ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class, otroArchivo.class);
    	//Otro ejemplo
    	/*AnnotationConfigApplicationContext  context = new AnnotationConfigApplicationContext(AppConfig.class);
        context.register(archivo1)
        context.refresh();*/
        
        Persona persona = context.getBean(Persona.class);
        System.out.println("Persona " + persona.getId() + " " + persona.getNombre() + " " + persona.getApodo() + " " + persona.getPais().getNombre()
        		+ " " + persona.getPais().getCiudad().getNombre());
    }
}
