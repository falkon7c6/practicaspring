package com.falkon.springTutorial.beans;

import org.springframework.beans.factory.annotation.Value;

public class HelloWorld {

	@Value("Hola Mundo") //anotacion para dar un valor por defecto
	private String saludo;

	public String getSaludo() {
		return saludo;
	}

	public void setSaludo(String saludo) {
		this.saludo = saludo;
	}
	
	public void saludar(){
		System.out.println(saludo);
	}

}